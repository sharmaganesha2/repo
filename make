#!/bin/sh
mkdir $1
cd $1
echo "#" $1 > README.md
wget https://raw.githubusercontent.com/licenses/license-templates/master/templates/x11.txt -qO LICENSE
cmtrepo > /dev/null
cd - > /dev/null
echo "Sucessfully made repo!"
