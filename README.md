# Repo

An alternative to Git. All it does is make a list of files.

## Installation
To install Repo, run:
```bash
chmod +x install
sudo ./install
```
## Usage
### Counterpart to `git init`
To make an empty repo, run:
```bash
mkrepo /path/to/new/repo
```
To initalize an existing directory, rename your License and Readme files. To do that, run:
```bash
mkrepo .
mv /path/to/old/license ./LICENSE.1
mv /path/to/old/readme/doc ./README.md.1
rm LICENSE
rm README.md
mv README.md.1 README.md
mv LICENSE.1 LICENSE
```
### Counterpart to `git commit`
To commit a repo, run:
```bash
cmtrepo
```
So, for a repo, you switched to Git. How do I "derepoify" this repo?
Just do:
```bash
rm -f .flist
```
To degitify a repo,
Just do:
```bash
rm -rf .git
```
